
           


            @include (' dash.links')
            

            <body class="">
                <div class="wrapper">
                    <div class="sidebar" data-color="purple" data-background-color="white" data-image="">
                        <!--
                    Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
            
                    Tip 2: you can also add an image using data-image tag
                -->
                        <div class="logo">
                            <a href="http://www.creative-tim.com" class="simple-text logo-normal">
                                Welcome back
                            </a>
                        </div>
                        
            
                        @include (' dash.aside')
            
                    </div>
                    <div class="main-panel">
                        <!-- Navbar -->
                        <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute fixed-top">
                            <div class="container-fluid">
                                <div class="navbar-wrapper">
                                    <a class="navbar-brand" href="#pablo"> User Profile</a>
                                </div>
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="navbar-toggler-icon icon-bar"></span>
                                    <span class="navbar-toggler-icon icon-bar"></span>
                                    <span class="navbar-toggler-icon icon-bar"></span>
                                </button>
                                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                                    <form class="navbar-form">
                                        <div class="input-group no-border">
                                            <input type="text" value="" class="form-control" placeholder="Search...">
                                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                                <i class="material-icons">search</i>
                                                <div class="ripple-container"></div>
                                            </button>
                                        </div>
                                    </form>
                                    <ul class="navbar-nav">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#pablo">
                                                <i class="material-icons">dashboard</i>
                                                <p>
                                                    <span class="d-lg-none d-md-block">Stats</span>
                                                </p>
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="material-icons">notifications</i>
                                                <span class="notification">5</span>
                                                <p>
                                                    <span class="d-lg-none d-md-block">Some Actions</span>
                                                </p>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                                <a class="dropdown-item" href="#">Mike John responded to your email</a>
                                                <a class="dropdown-item" href="#">You have 5 new tasks</a>
                                                <a class="dropdown-item" href="#">You are now friend with Andrew</a>
                                                <a class="dropdown-item" href="#">Another Notification</a>
                                                <a class="dropdown-item" href="#">Another One</a>
                                            </div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#pablo">
                                                <i class="material-icons">person</i>
                                                <p>
                                                    <span class="d-lg-none d-md-block">Account</span>
                                                </p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                        <!-- End Navbar -->
                        <div class="content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="card">
                                            <div class="card-header card-header-primary">
                                                <h4 class="card-title">Edit Profile</h4>
                                                <p class="card-category">Complete your profile</p>
                                            </div>
                                            <div class="card-body">
                                                <form>
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Company (disabled)</label>
                                                                <input type="text" class="form-control" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Username</label>
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Email address</label>
                                                                <input type="email" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Fist Name</label>
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Last Name</label>
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Adress</label>
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">City</label>
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Country</label>
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Postal Code</label>
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>About Me</label>
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating"> Lamborghini Mercy, Your chick she so thirsty, Iam in that two seat Lambo.</label>
                                                                    <textarea class="form-control" rows="5"> </textarea >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary pull-right">Update Profile</button>
                                                    <div class="clearfix"></div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-profile">
                                            <div class="card-avatar">
                                                <a href="#pablo">
                                                    <img class="img" src="" />
                                                </a>
                                            </div>
                                            <div class="card-body">
                                                <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                                                <h4 class="card-title"> joshuas mom</h4>
                                                <p class="card-description">

                                                </p>
                                                <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="footer ">
                            <div class="container-fluid">
                                <nav class="pull-left">
                                    <ul>
                                        <li>
                                            <a href="https://www.creative-tim.com">
                                                Creative Tim
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://presentation.creative-tim.com">
                                                About Us
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://blog.creative-tim.com">
                                                Blog
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.creative-tim.com/license">
                                                Licenses
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                                <div class="copyright pull-right">
                                    &copy;
                                    <script>
                                        document.write(new Date().getFullYear())
                                    </script>, made with love by
                                    <a href="https://www.creative-tim.com" target="_blank">Alonge Tolu</a> for a better web.
                                </div>
                            </div>
                        </footer>
                    </div>
                