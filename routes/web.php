<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'usercontroller@home' )->name('home');
Route::post('/t', 'usercontroller@postsigin' );


// signup routes
Route::get('/si', 'usercontroller@getsignup' )->name('users.signup');
Route::post('/si', 'usercontroller@postsignup' )->name('users.signup');


//profile
Route::get('/p', 'usercontroller@profile' )->name('profile');


//dashbord routes
Route::get('/dash', 'dashcontroller@index' )->name('dash');
Route::get('/dash/profile', 'dashcontroller@getindex' )->name('dash.profile');



